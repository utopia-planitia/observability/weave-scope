
all: manifests/helmfile.yaml

manifests/helmfile.yaml: helmfile.yaml
	helmfile template \
		| msort \
		> manifests/helmfile.yaml
