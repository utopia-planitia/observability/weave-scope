apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: weave
  namespace: weave-scope
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-realm: "Authentication Required"
spec:
  tls:
    - hosts:
        - weave-scope.{{ .Cluster.Domain }}
      secretName: {{ .Cluster.Name }}-tls
  rules:
    - host: weave-scope.{{ .Cluster.Domain }}
      http:
        paths:
          - path: /
            backend:
              serviceName: weave-scope-weave-scope
              servicePort: http
---
apiVersion: v1
kind: Secret
metadata:
  name: basic-auth
  namespace: weave-scope
type: Opaque
data:
  auth: {{ .Cluster.BasicAuth | b64encode }}
