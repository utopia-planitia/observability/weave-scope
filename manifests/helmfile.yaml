---
# Source: weave-scope/charts/weave-scope-cluster-agent/templates/clusterrole.yaml
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  labels:    
    chart: weave-scope-cluster-agent-1.1.10
    heritage: Helm
    release: weave-scope
    app: weave-scope
    component: agent
  name: weave-scope-cluster-agent-weave-scope 
  annotations:    
    cloud.weave.works/launcher-info: |-
      {
        "server-version": "master-4fe8efe",
        "original-request": {
          "url": "/k8s/v1.7/scope.yaml"
        },
        "email-address": "support@weave.works",
        "source-app": "weave-scope",
        "weave-cloud-component": "scope"
      }
rules:
  - apiGroups:
      - '*'
    resources:
      - '*'
    verbs:
      - '*'
  - nonResourceURLs:
      - '*'
    verbs:
      - '*'
---
# Source: weave-scope/charts/weave-scope-cluster-agent/templates/clusterrolebinding.yaml
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  labels:    
    chart: weave-scope-cluster-agent-1.1.10
    heritage: Helm
    release: weave-scope
    app: weave-scope
    component: agent
  name: weave-scope-weave-scope
  annotations:    
    cloud.weave.works/launcher-info: |-
      {
        "server-version": "master-4fe8efe",
        "original-request": {
          "url": "/k8s/v1.7/scope.yaml"
        },
        "email-address": "support@weave.works",
        "source-app": "weave-scope",
        "weave-cloud-component": "scope"
      }
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: weave-scope-cluster-agent-weave-scope
subjects:
  - kind: ServiceAccount
    name: weave-scope-cluster-agent-weave-scope
    namespace: weave-scope
---
# Source: weave-scope/charts/weave-scope-agent/templates/daemonset.yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  labels:    
    chart: weave-scope-agent-1.1.10
    heritage: Helm
    release: weave-scope
    app: weave-scope
    component: agent
  name: weave-scope-agent-weave-scope
  annotations:    
    cloud.weave.works/launcher-info: |-
      {
        "server-version": "master-4fe8efe",
        "original-request": {
          "url": "/k8s/v1.7/scope.yaml"
        },
        "email-address": "support@weave.works",
        "source-app": "weave-scope",
        "weave-cloud-component": "scope"
      }
spec:
  selector:
    matchLabels:
      app: weave-scope
      release: weave-scope
      component: agent
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:        
        chart: weave-scope-agent-1.1.10
        heritage: Helm
        release: weave-scope
        app: weave-scope
        component: agent
    spec:
      tolerations:
        - effect: NoSchedule
          operator: Exists
      containers:
        - name: weave-scope-agent
          image: "weaveworks/scope:1.12.0"
          imagePullPolicy: "IfNotPresent"
          args:
            - '--mode=probe'
            - '--probe-only'
            - '--probe.kubernetes.role=host'
            - '--probe.docker.bridge=docker0'
            - '--probe.docker=true'
            - '--probe.kubernetes=true'
            
            - weave-scope-weave-scope.weave-scope.svc:80
          securityContext:
            privileged: true
          resources:
            null
          volumeMounts:
            - name: docker-socket
              mountPath: /var/run/docker.sock
            - name: scope-plugins
              mountPath: /var/run/scope/plugins
            - name: sys-kernel-debug
              mountPath: /sys/kernel/debug
      volumes:
        - name: docker-socket
          hostPath:
            path: /var/run/docker.sock
        - name: scope-plugins
          hostPath:
            path: /var/run/scope/plugins
        - name: sys-kernel-debug
          hostPath:
            path: /sys/kernel/debug
      hostPID: true
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet
---
# Source: weave-scope/charts/weave-scope-cluster-agent/templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:    
    chart: weave-scope-cluster-agent-1.1.10
    heritage: Helm
    release: weave-scope
    app: weave-scope
    component: cluster-agent
  name: weave-scope-cluster-agent-weave-scope
  annotations:    
    cloud.weave.works/launcher-info: |-
      {
        "server-version": "master-4fe8efe",
        "original-request": {
          "url": "/k8s/v1.7/scope.yaml"
        },
        "email-address": "support@weave.works",
        "source-app": "weave-scope",
        "weave-cloud-component": "scope"
      }
spec:
  selector:
    matchLabels:
      app: weave-scope
      release: weave-scope
      component: cluster-agent
  strategy:
    type: RollingUpdate
  template:
    metadata:
      labels:        
        chart: weave-scope-cluster-agent-1.1.10
        heritage: Helm
        release: weave-scope
        app: weave-scope
        component: cluster-agent
    spec:
      containers:
        - name: weave-scope-cluster-agent
          image: "weaveworks/scope:1.12.0"
          imagePullPolicy: "IfNotPresent"
          args:
            - '--mode=probe'
            - '--probe-only'
            - '--probe.kubernetes.role=cluster'
            
            - weave-scope-weave-scope.weave-scope.svc:80
          resources:
            null
      serviceAccountName: weave-scope-cluster-agent-weave-scope
---
# Source: weave-scope/charts/weave-scope-frontend/templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:    
    chart: weave-scope-frontend-1.1.10
    heritage: Helm
    release: weave-scope
    app: weave-scope
    component: frontend
  name: weave-scope-frontend-weave-scope
  annotations:    
    cloud.weave.works/launcher-info: |-
      {
        "server-version": "master-4fe8efe",
        "original-request": {
          "url": "/k8s/v1.7/scope.yaml"
        },
        "email-address": "support@weave.works",
        "source-app": "weave-scope",
        "weave-cloud-component": "scope"
      }
spec:
  replicas: 1
  selector:
    matchLabels:
      app: weave-scope
      release: weave-scope
      component: frontend
  template:
    metadata:
      labels:        
        chart: weave-scope-frontend-1.1.10
        heritage: Helm
        release: weave-scope
        app: weave-scope
        component: frontend
    spec:
      containers:
        - name: weave-scope-frontend
          image: "weaveworks/scope:1.12.0"
          imagePullPolicy: "IfNotPresent"
          args:
            - "--no-probe"
          ports:
            - name: http
              containerPort: 4040
              protocol: TCP
          resources:
            null
---
# Source: weave-scope/charts/weave-scope-frontend/templates/service.yaml
apiVersion: v1
kind: Service
metadata:
  labels:    
    chart: weave-scope-frontend-1.1.10
    heritage: Helm
    release: weave-scope
    app: weave-scope
    component: frontend
  name: weave-scope-weave-scope
  annotations:    
    cloud.weave.works/launcher-info: |-
      {
        "server-version": "master-4fe8efe",
        "original-request": {
          "url": "/k8s/v1.7/scope.yaml"
        },
        "email-address": "support@weave.works",
        "source-app": "weave-scope",
        "weave-cloud-component": "scope"
      }
spec:
  ports:
    - name: http
      port: 80
      targetPort: http
      protocol: TCP
  selector:
    app: weave-scope
    release: weave-scope
    component: frontend
  type: ClusterIP
---
# Source: weave-scope/charts/weave-scope-cluster-agent/templates/serviceaccount.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:    
    chart: weave-scope-cluster-agent-1.1.10
    heritage: Helm
    release: weave-scope
    app: weave-scope
    component: agent
  name: weave-scope-cluster-agent-weave-scope
  annotations:    
    cloud.weave.works/launcher-info: |-
      {
        "server-version": "master-4fe8efe",
        "original-request": {
          "url": "/k8s/v1.7/scope.yaml"
        },
        "email-address": "support@weave.works",
        "source-app": "weave-scope",
        "weave-cloud-component": "scope"
      }
